﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {

        static void Main(string[] args)
        {
            string[] str = new string[3] { "alma ", "szilva ", "a fa alatt" };

            Console.WriteLine(str[0] + str[1] + str[2]);

            var str1 = str[0] + ", " + str[1] + ", " + str[2];

            Console.WriteLine($"Ennyi karakterből áll a szöveg:  { str1.Count() }");
            Console.WriteLine($"Az első karakter: {str1.First() }");
            Console.WriteLine($"Az első karakter: {str1.Last()}");

            Console.WriteLine($"szilva szöveggel kezdődik-e? {str1.StartsWith("szilva")}");

            Console.WriteLine($"tt-re végződik? {str1.EndsWith("tt")}");
            Console.WriteLine($"Tartalmazza-e azt, hogy fa? {str1.Contains("fa")}");
            Console.WriteLine($"Hanyadik indexű elem az f? {str1.IndexOf('f')}");

            str1.Split(',');

            Console.WriteLine($"Nagy betűkkel: {str1.ToUpper()}");
            Console.WriteLine($"Kis betűkkel: {str1.ToLower()}");

            Console.WriteLine($"{str1.Replace('a', 'e')}");
            Console.WriteLine($"{str1.Remove(13)}");
            Console.WriteLine(str1.Substring(6).Replace('s', 'S'));
   
            Console.ReadKey();


        }
    }
}